<?php

require_once 'Payment.php';
require_once 'Transaction.php';
require_once 'User.php';
require_once 'utils.php';

define('DEFAULT_ACCOUNT', 'rPjgkFQqgj9HVr99VkXv3Ri5rQZMmkdNLj');
define('DEFAULT_SECRET', 'shpZuHmyr4dMnEmeGUj1qEuTp7wW4');
define('DEFAULT_RECIPIENT', 'r9U9DDht72oMx7nrqsS7uELXNvfsYL4USm');
define('DEFAULT_AMOUNT_VALUE', '11');
define('DEFAULT_DESTINATION_TAG', '999');

/**
 *
 * Если не переданы параметры, подставляются дефолтный
 *
 * Пример вызова с переданными параметрами account, secret, recipient, amount и destination tag:
 * php index.php rPjgkFQqgj9HVr99VkXv3Ri5rQZMmkdNLj shpZuHmyr4dMnEmeGUj1qEuTp7wW4 r9U9DDht72oMx7nrqsS7uELXNvfsYL4USm 11 999
 *
 * Пример вызова без переданных параметров:
 * php index.php
 *
 */
$account = getAccount() ?: DEFAULT_ACCOUNT;
$secret = getSecret() ?: DEFAULT_SECRET;
$recipient = getRecipient() ?: DEFAULT_RECIPIENT;
$amountValue = getAmountValue() ?: DEFAULT_AMOUNT_VALUE;
$destinationTag = getDestinationTag() ?: DEFAULT_DESTINATION_TAG;

$user = new User($account, $secret);
$payment = new Payment($recipient, $amountValue, $destinationTag);

$transaction = new Transaction($user, $payment);
$transaction->send();
$transaction->getResponse();