<?php

class Payment
{
    public $recipient;
    public $amountValue;
    public $destinationTag;

    public function __construct($recipient, $amountValue, $destinationTag)
    {
        $this->recipient = $recipient;
        $this->amountValue = (int) $amountValue;
        $this->destinationTag = (int) $destinationTag;
    }
}
