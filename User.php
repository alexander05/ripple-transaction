<?php

class User
{
    public $account;
    public $secret;

    public function __construct($account, $secret)
    {
        $this->account = $account;
        $this->secret = $secret;
    }
}