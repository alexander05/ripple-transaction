<?php

require_once './wss/ConnectionException.php';
require_once './wss/IWscCommons.php';
require_once './wss/WscMain.php';
require_once './wss/WebSocketClient.php';

use WSSC\WebSocketClient;

class Transaction
{
    private $account;
    private $secret;
    private $amount;
    private $destination;
    private $destinationTag;
    private $transactionType;
    private $response;

    const TYPE_PAYMENT = 'Payment';
    const STATUS_SUCCESS = 'success';

    public function __construct(User $user, Payment $payment)
    {
        $this->account = $user->account;
        $this->secret = $user->secret;

        $this->amount = (floor($payment->amountValue) * 1e6);
        $this->destination = $payment->recipient;
        $this->destinationTag = (int) $payment->destinationTag;
        $this->transactionType = self::TYPE_PAYMENT;
    }

    public function send()
    {
        $client = new WebSocketClient('wss://s.altnet.rippletest.net:51233');
        $client->send($this->json());
        $this->response = $client->receive();
    }

    public function getResponse()
    {
        if ($this->response->status === self::STATUS_SUCCESS){
            echo
                'result: ' . $this->response->result->engine_result_message . "\n\r\n\r" .
                'tx_blob: ' . $this->response->result->tx_blob . "\n\r\n\r" .
                'hash: ' . $this->response->result->tx_json->hash;
        } else {
            echo
                'result: ' . $this->response->status . "\n\r" .
                'message: ' . $this->response->error_message;
        }
    }

    private function json()
    {
        $data = [
            'command' => 'submit',
            'tx_json' => [
                'Account' => $this->account,
                'Amount' => $this->amount,
                'Destination' => $this->destination,
                'DestinationTag' => $this->destinationTag,
                'TransactionType' => $this->transactionType,
            ],
            'secret' => $this->secret
        ];

        return json_encode($data);
    }
}
