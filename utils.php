<?php

function getAccount()
{
    if (array_key_exists(1, $_SERVER['argv'])){
        return $_SERVER['argv'][1];
    }

    return null;
}

function getSecret()
{
    if (array_key_exists(2, $_SERVER['argv'])){
        return $_SERVER['argv'][2];
    }

    return null;
}

function getRecipient()
{
    if (array_key_exists(3, $_SERVER['argv'])){
        return $_SERVER['argv'][3];
    }

    return null;
}

function getAmountValue()
{
    if (array_key_exists(4, $_SERVER['argv'])){
        return $_SERVER['argv'][4];
    }

    return null;
}

function getDestinationTag()
{
    if (array_key_exists(5, $_SERVER['argv'])){
        return $_SERVER['argv'][5];
    }

    return null;
}
